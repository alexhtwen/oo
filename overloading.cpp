#include <iostream>
using namespace std;

class Tree {
    public:
        string breed;

        Tree(string myBreed){
            breed = myBreed;
        }

    // Overloading: 以下兩個blossom()函數名稱相同，但參數不同，即signature不同。
    // C++視作不同的函數。
    void blossom(string startDate, string endDate){
        cout << "The blooming season of peaches will start from " << startDate << " till " << endDate << " this year." << endl;
    }

    // 切記：signature是函數名稱加上參數，不包括傳回值。
    void blossom(string month){
        cout << this->breed << " blossoms in " << month << '.' << endl;
    }
};


int main(void) {
    Tree tree("KV Plum");
    tree.blossom("March 6", "April 28");  // 呼叫兩個參數的blossom()。
    cout << endl;
    tree.blossom("May");                  // 呼叫只有一個參數的blossom()。
    return 0;
}