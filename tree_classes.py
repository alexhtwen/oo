# tree_classes.py   # 以後再分拆成一個類別一個.py檔。
from abc import ABC, abstractmethod

class Tree(ABC):
    def __init__(self, breed: str, age: int):   # constructor
        is_valid_breed = True   # 判斷條件略。
        if is_valid_breed:
            self.__breed = breed
        else:
            raise Exception('Invalid breed.')

        is_valid_age = True   # 判斷條件略。
        if is_valid_age:
            self.__age = age
        else:
            raise Exception('Invalid age.')

    @property
    def breed(self) -> str:
        return self.__breed

    @abstractmethod
    def grow(self):        # 生長(樹的共同行為)
        ...

    @abstractmethod        # 繁殖(樹的共同行為)
    def reproduce(self):
        ...

    @abstractmethod        # 生病(樹的共同行為)
    def get_sick(self):
        ...

    @abstractmethod        # 死亡(樹的共同行為)
    def die(self):
        ...


class EarthlyTree(Tree):   # 地球上的樹
    def __init__(self, breed: str, age: int):   # constructor
        is_valid_breed = True   # 判斷條件略。
        if is_valid_breed:
            self.__breed = breed
        else:
            raise Exception('Invalid breed.')

        is_valid_age = True   # 判斷條件略。
        if is_valid_age:
            self.__age = age
        else:
            raise Exception('Invalid age.')

    @property
    def breed(self) -> str:
        return self.__breed

    @breed.setter
    def breed(self, breed: str):
        is_valid_breed = True   # 判斷條件略。
        if is_valid_breed:
            self.__breed = breed
        else:
            raise Exception('Invalid breed.')

    @property
    def age(self) -> int:
        return self.__age

    @age.setter
    def age(self, age: int):
        is_valid_age = True   # 判斷條件略。
        if is_valid_age:
            self.__age = age
        else:
            raise Exception('Invalid age.')

    def grow(self):        # 生長。
        print(f'{__class__.__name__} {self.breed} is growing.')

    def reproduce(self):   # 繁殖。
        print(f'{__class__.__name__} {self.breed} is reproducing.')

    def get_sick(self):     # 得病。
        print(f'{__class__.__name__} {self.breed} is getting sick.')

    def die(self):          # 死亡(假設死亡也有「行為」)。
        print(f'{__class__.__name__} {self.breed} is dying.')


class VenusianTree(Tree):   # 金星樹
    def __init__(self, breed: str, age: int):   # constructor
        is_valid_breed = True   # 判斷條件略。
        if is_valid_breed:
            self.__breed = breed
        else:
            raise Exception('Invalid breed.')

        is_valid_age = True   # 判斷條件略。
        if is_valid_age:
            self.__age = age
        else:
            raise Exception('Invalid age.')

    @property
    def breed(self) -> str:
        return self.__breed

    @breed.setter
    def breed(self, breed: str):
        is_valid_breed = True   # 判斷條件略。
        if is_valid_breed:
            self.__breed = breed
        else:
            raise Exception('Invalid breed.')

    @property
    def age(self) -> int:
        return self.__age

    @age.setter
    def age(self, age: int):
        is_valid_age = True   # 判斷條件略。
        if is_valid_age:
            self.__age = age
        else:
            raise Exception('Invalid age.')

    def grow(self):        # 生長。
        print(f'{__class__.__name__} {self.breed} grows younger and younger.')

    def reproduce(self):   # 繁殖。
        print(f'{__class__.__name__} {self.breed} reproduces on a daily basis.')

    def get_sick(self):     # 得病。
        print(f'{__class__.__name__} {self.breed} always revovers from illness.')

    def die(self):          # 死亡(假設死亡也有「行為」)。
        print(f'{__class__.__name__} {self.breed} raises itself from the dead.')


class MartianTree(Tree):   # 火星樹
    def __init__(self, breed: str, age: int):   # constructor
        is_valid_breed = True   # 判斷條件略。
        if is_valid_breed:
            self.__breed = breed
        else:
            raise Exception('Invalid breed.')

        is_valid_age = True   # 判斷條件略。
        if is_valid_age:
            self.__age = age
        else:
            raise Exception('Invalid age.')

    @property
    def breed(self) -> str:
        return self.__breed

    @breed.setter
    def breed(self, breed: str):
        is_valid_breed = True   # 判斷條件略。
        if is_valid_breed:
            self.__breed = breed
        else:
            raise Exception('Invalid breed.')

    @property
    def age(self) -> int:
        return self.__age

    @age.setter
    def age(self, age: int):
        is_valid_age = True   # 判斷條件略。
        if is_valid_age:
            self.__age = age
        else:
            raise Exception('Invalid age.')

    def grow(self):        # 生長。
        print(f'{__class__.__name__} {self.breed} stops growing.')

    def reproduce(self):   # 繁殖。
        print(f'{__class__.__name__} {self.breed} does not reproduce.')

    def get_sick(self):     # 得病。
        print(f'{__class__.__name__} {self.breed} is healthy.')

    def die(self):          # 死亡(假設死亡也有「行為」)。
        print(f'{__class__.__name__} {self.breed} is immortal.')

    def jog(self):          # 慢跑是火星樹的獨特行為。
        print(f'{__class__.__name__} {self.breed} is jogging.')


class SolarTree(Tree):     # 太陽樹
    def __init__(self, breed: str, age: int):   # constructor
        is_valid_breed = True   # 判斷條件略。
        if is_valid_breed:
            self.__breed = breed
        else:
            raise Exception('Invalid breed.')
        is_valid_age = True   # 判斷條件略。
        if is_valid_age:
            self.__age = age
        else:
            raise Exception('Invalid age.')

    @property
    def breed(self) -> str:
        return self.__breed

    @breed.setter
    def breed(self, breed: str):
        is_valid_breed = True   # 判斷條件略。
        if is_valid_breed:
            self.__breed = breed
        else:
            raise Exception('Invalid breed.')

    @property
    def age(self) -> int:
        return self.__age

    @age.setter
    def age(self, age: int):
        is_valid_age = True   # 判斷條件略。
        if is_valid_age:
            self.__age = age
        else:
            raise Exception('Invalid age.')

    def grow(self):        # 生長。
        print(f'{__class__.__name__} {self.breed} grows for a billion years.')

    def reproduce(self):   # 繁殖。
        print(f'{__class__.__name__} {self.breed} reproduces nuclear power.')

    def get_sick(self):     # 得病。
        print(f'{__class__.__name__} {self.breed} never gets sick.')

    def die(self):          # 死亡(假設死亡也有「行為」)。
        print(f'{__class__.__name__} {self.breed} will die in 7.5 billion years.')

    def nuclear_fuse(self):          # 核融合(聚變)是太陽樹的獨特行為。
        print(f'{__class__.__name__} {self.breed} operates nuclear fusion.')
