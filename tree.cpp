#include <iostream>
using namespace std;
class Tree
{
    public:
        string breed;   // public attribute(公開屬性)

        // constructor建構子(也是公開)。
        Tree(string breed, int age, int height)
        {
            cout << "．在Constructor中..." << endl;
            this->breed = breed;
            this->age = age;
            this->height = height;
            cout << "  所有屬性的初值都已設定。" << endl;
        }

        void ShowInfo()  // public method(公開方法)
        {
            cout << "  breed(public)  : " << breed << endl;
            cout << "  age(protected) : " << age << endl;
            cout << "  height(private): " << height << endl;
        }

    protected:
        int age;

    private:
        int height;
};

int main()
{
    // Create an instance(object) without using the 'new' operator.
    Tree Laozi("cedar", 2953, 59);

    // C++的constructor只能在「創建物件」時由C++的compiler自動調用，不可外顯呼叫。
    // Laozi.Tree("cedar", 2953, 59);  // 這樣會引發執行時期錯誤。

    cout << "\n．透過呼叫Laozi.ShowInfo()方法取值。protected/private的屬性均可存取：\n";
    Laozi.ShowInfo();

    cout << "\n．用object.attribute(物件.屬性)表示法取值。只能存取public層級的屬性：\n";
    cout << "  Laozi's breed(public)  : " << Laozi.breed << endl;
    // cout << "  Laozi's age(protected) : " << Laozi.age << endl;
    // cout << "  Laozi's height(private): " << Laozi.height << endl;
}