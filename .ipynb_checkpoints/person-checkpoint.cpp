#include <iostream>
using namespace std;
class Tree
{
public:
    string name; // public attribute(公開屬性)

    // constructor建構子。C++還有descturtor解構子，不過Python沒有，就不講了。
    Tree(string name, string hair, string dob)
    {
        cout << "．在Constructor中..." << endl;
        this->name = name;
        this->hair = hair;
        this->dob = dob;
        cout << "  所有屬性的初值都已設定。" << endl;
    }

    void ShowInfo() // public method(公開方法)
    {
        cout << "  name(public):           " << name << endl;
        cout << "  hair color(protected):  " << hair << endl;
        cout << "  date of birth(private): " << dob << endl;
    }

protected:
    string hair;

private:
    string dob;
};

int main()
{
    // Create an instance(object) without using the 'new' operator.
    Tree Bombshell("Marilyn Monroe", "blonde", "1926-06-01");

    // C++的constructor只能在「創建物件」時由C++的compiler自動調用，不可外顯呼叫。
    // Bombshell.Person("Asing", "gray", "1972-05-03");

    cout << "\n．透過呼叫Bombshell.ShowInfo()方法取值。protected/private的屬性均可存取：\n";
    Bombshell.ShowInfo();

    cout << "\n．用object.attribute(物件.屬性)表示法取值。只能存取public層級的屬性：\n";
    cout << "  Bombshell's full name(public): " << Bombshell.name << endl;
    // cout << "    Bombshell's hair color(protected): " << Bombshell.hair << endl;
    // cout << "    Bombshell's date of birth(private): " << Bombshell.dob << endl;
}