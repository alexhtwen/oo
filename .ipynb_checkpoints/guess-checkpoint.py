import random


def guess_num(ans):  # 猜大小遊戲
    print("ans:", ans)
    while True:
        guess = int(input("\n請輸入一個1-100間的數字(-1 不玩了)："))
        if guess < 0:
            return -1
        if guess > ans:
            print("您猜的數字太大，請再試。")
        elif guess < ans:
            print("您猜的數字太小，請再試。")
        else:
            print("Bingo.\n")
            break
    return 0

def func(a, b, c, d, e):
    pass

func([123, 234, 456, 987, 876], 123, 567, 'Alex Van...', (3, 4, 5, 6, 7))





for _ in range(999999):
    if guess_num(random.choice(range(1, 101))) == -1:
        print("Game Over.\n")
        break