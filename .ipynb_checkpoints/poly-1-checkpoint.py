from pathlib import Path
class AudioFile:
    extensions = ('wav', 'mp3', 'wma', 'mp4', 'ogg')
    def __init__(self, filename):
        self.file_info = {'name': None, 'extension': None}
        try:
            extension = Path(filename).suffix[1:].lower()
            if extension not in self.extensions:
                self.file_info['name'] = filename
                raise Exception('Invalid file format.')

            else:
#                 print(f'Correct.  filename: {filename}{" "*5}format: {extension}')
                self.file_info['name'] = filename
                self.file_info['extension'] = extension
                return
        except Exception as e:
            print(e)
            return
        finally:
            return

audio = AudioFile('my_file.mp4')
print(audio.file_info)

class WavFile(AudioFile):
    ext = 'wav'
    def play(self):
        print(f'I am playing {self.file_info["name"]} using {self.file_info["extension"]} format.')

mp3 = WavFile('play_me.wav')
mp3.play()

class Mp3File(AudioFile):
    ext = 'mp3'
    def play(self):
        print(f'I am playing {self.file_info["name"]} using {self.file_info["extension"]} format.')

mp3 = Mp3File('play_me.mp3')
mp3.play()

class WmaFile(AudioFile):
    ext = 'wma'
    def play(self):
        print(f'I am playing {self.file_info["name"]} using {self.file_info["extension"]} format.')

mp3 = Mp3File('play_me.wma')
mp3.play()

class Mp4File(AudioFile):
    ext = 'mp4'
    def play(self):
        print(f'I am playing {self.file_info["name"]} using {self.file_info["extension"]} format.')

mp3 = Mp3File('play_me.mp4')
mp3.play()

class OggFile(AudioFile):
    ext = 'ogg'
    def play(self):
        print(f'I am playing {self.file_info["name"]} using {self.file_info["extension"]} format.')

mp3 = Mp3File('play_me.ogg')
mp3.play()