

'''Alex Van 温華添'''
class Me():    # NoCo.
    'Python, backend, tutoring, ...'
    email = 'alexhtwen@gmail.com'
    github = 'https://github.com/alexhtwen'
    phone = '+886 918-800878'
    @staticmethod
    def greet(word: str) -> None:
        print(f'{word}from {__doc__}')
Me.greet("  'Hello, World!' ")
