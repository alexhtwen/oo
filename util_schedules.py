def get_user_schedules():
    out_values = [6, 2, 5, 9, 0]
    check = -9999
    try:
        # 3 / 0  # would raise an exception
        out_values[next((i for i, value in enumerate(out_values)
                         if value == check))] = 'Bingo'
        return out_values
    except Exception as e:
        print('-----------')
        print(f'Error: {e.__repr__()}')
        print('-----------')
        out_values['status'] = 'failed'
        out_values['message'] = str(e)
        return out_values
