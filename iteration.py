from collections.abc import Iterable, Iterator

x_0 = [1, 2, 3]
print(isinstance(x_0, Iterable), isinstance(x_0, Iterator))
# True False

x_1 = (1, 2, 3)
print(isinstance(x_1, Iterable), isinstance(x_1, Iterator))
# True False

x_2 = range(3)
print(isinstance(x_2, Iterable), isinstance(x_2, Iterator))
# True False

x_3 = "123"
print(isinstance(x_3, Iterable), isinstance(x_3, Iterator))
# True False