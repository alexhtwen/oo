FROM python:3.10.2

# WORKDIR /jup

RUN python -m pip install --upgrade pip
RUN pip install jupyter -U && pip install jupyterlab isort black && \
    pip install nbresuse xeus-python nbconvert pandas matplotlib && \
    pip install jupyterlab_latex jupyterlab-python-file lckr-jupyterlab-variableinspector && \
    pip install jupyterlab_code_formatter jupyterlab-spreadsheet-editor ipympl && \
    pip install jupyterthemes && \
    pip install --ignore-installed pyzmq && \
    pip install --ignore-installed jupyter-core && \
    pip install --ignore-installed jupyter-client

RUN jt -t monokai -f inconsolata -fs 11 -cellw 95% -nfs 11 -tfs 11 -ofs 10 -dfs 10 -T -N

EXPOSE 8888
# EXPOSE 5555

ENTRYPOINT ["jupyter", "lab","--ip=0.0.0.0","--allow-root"]