from datetime import datetime


class dt():
    @classmethod
    def now(cls):
        return datetime.now()

    # # @classmethod
    # def d(self):
    #     return datetime.now().date()

    def __init__(self):
        ...


print(f'{type(dt)=}')
this_moment = dt.now()
print(f'{type(this_moment)=}')
print(f'{type(this_moment.date())=}')

# s = 'alex'
# print(type(s))