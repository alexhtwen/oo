class EarthlyTree():   # 地球上的樹
    def __init__(self, breed: str, age: int):   # constructor
        is_valid_breed = True   # 判斷條件略。
        if is_valid_breed:
            self.__breed = breed
        else:
            raise Exception('Invalid breed.')

        is_valid_age = True   # 判斷條件略。
        if is_valid_age:
            self.__age = age
        else:
            raise Exception('Invalid age.')

    @property
    def breed(self) -> str:
        return self.__breed

    @breed.setter
    def breed(self, breed: str):
        is_valid_breed = True   # 判斷條件略。
        if is_valid_breed:
            self.__breed = breed
        else:
            raise Exception('Invalid breed.')

    @property
    def age(self) -> int:
        return self.__age

    @age.setter
    def age(self, breed: str):
        is_valid_age = True   # 判斷條件略。
        if is_valid_age:
            self.__age = age
        else:
            raise Exception('Invalid age.')

    def grow(self):        # 生長。
        print(f'{__class__.__name__} {self.breed} is growing.')

    def reproduce(self):   # 繁殖。
        print(f'{__class__.__name__} {self.breed} is reproducing.')

    def get_sick(self):     # 得病。
        print(f'{__class__.__name__} {self.breed} is getting sick.')

    def die(self):          # 死亡(假設死亡也有「行為」)。
        print(f'{__class__.__name__} {self.breed} is dying.')


class VenusianTree():   # 金星樹
    def __init__(self, breed: str, age: int):   # constructor
        is_valid_breed = True   # 判斷條件略。
        if is_valid_breed:
            self.__breed = breed
        else:
            raise Exception('Invalid breed.')

        is_valid_age = True   # 判斷條件略。
        if is_valid_age:
            self.__age = age
        else:
            raise Exception('Invalid age.')

    @property
    def breed(self) -> str:
        return self.__breed

    @breed.setter
    def breed(self, breed: str):
        is_valid_breed = True   # 判斷條件略。
        if is_valid_breed:
            self.__breed = breed
        else:
            raise Exception('Invalid breed.')

    @property
    def age(self) -> int:
        return self.__age

    @age.setter
    def age(self, breed: str):
        is_valid_age = True   # 判斷條件略。
        if is_valid_age:
            self.__age = age
        else:
            raise Exception('Invalid age.')

    def grow(self):        # 生長。
        print(f'{__class__.__name__} {self.breed} grows younger and younger.')

    def reproduce(self):   # 繁殖。
        print(f'{__class__.__name__} {self.breed} reproduces on a daily basis.')

    def get_sick(self):     # 得病。
        print(f'{__class__.__name__} {self.breed} always revovers from illness.')

    def die(self):          # 死亡(假設死亡也有「行為」)。
        print(f'{__class__.__name__} {self.breed} raises itself from the dead.')


class MartianTree():   # 火星樹
    def __init__(self, breed: str, age: int):   # constructor
        is_valid_breed = True   # 判斷條件略。
        if is_valid_breed:
            self.__breed = breed
        else:
            raise Exception('Invalid breed.')

        is_valid_age = True   # 判斷條件略。
        if is_valid_age:
            self.__age = age
        else:
            raise Exception('Invalid age.')

    @property
    def breed(self) -> str:
        return self.__breed

    @breed.setter
    def breed(self, breed: str):
        is_valid_breed = True   # 判斷條件略。
        if is_valid_breed:
            self.__breed = breed
        else:
            raise Exception('Invalid breed.')

    @property
    def age(self) -> int:
        return self.__age

    @age.setter
    def age(self, breed: str):
        is_valid_age = True   # 判斷條件略。
        if is_valid_age:
            self.__age = age
        else:
            raise Exception('Invalid age.')

    def grow(self):        # 生長。
        print(f'{__class__.__name__} {self.breed} stops growing.')

    def reproduce(self):   # 繁殖。
        print(f'{__class__.__name__} {self.breed} does not reproduce.')

    def get_sick(self):     # 得病。
        print(f'{__class__.__name__} {self.breed} is healthy.')

    def die(self):          # 死亡(假設死亡也有「行為」)。
        print(f'{__class__.__name__} {self.breed} is immortal.')


def universe(tree: object):
      tree.grow()
      tree.reproduce()
      tree.get_sick()
      tree.die()
    #   print(f'{tree.breed = :10}{tree.age = :<20,}')

earthly_tree = EarthlyTree('cedar', 2_500)
venusian_tree = VenusianTree('varan', -3_000)
martian_tree = MartianTree('mvule', 500_000_000)

print()
universe(earthly_tree)
print()
universe(venusian_tree)
print()
universe(martian_tree)
print()