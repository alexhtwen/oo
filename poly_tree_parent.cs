using System;
using System.ComponentModel;
namespace MyApplication
{
  class Tree  // Base class (parent)
  {
  }

  class Hardwood : Tree  // Derived class (child)
  {
  }

  class Conifer : Tree  // Derived class (child)
  {
  }

  class VenusianTree : Tree   // 金星上的樹
  {
  }

  class MartianTree : Tree   // 火星上的樹
  {
  }

  class SaturnianTree : Tree   // 土星上的樹
  {
  }

  class TreeDoctor
  {
    public void cure(Tree tree)  // 只要是「樹」我就能治。
    {
      Console.WriteLine("I am curing the " + tree + '.');
      Console.WriteLine("I am curing the " + System.ComponentModel.TypeDescriptor.GetClassName(tree) + '.');
    }
  }

  class Program
  {
    static void Main(string[] args)
    {
      Tree hardwood = new Hardwood();            // 形式型別是Tree，實際型別是Hardwood。
      Tree conifer = new Conifer();              // 形式型別是Tree，實際型別是Conifer。
      Tree venusianTree = new VenusianTree();    // 形式型別是Tree，實際型別是VenusianTree。
      Tree martianTree = new MartianTree();      // 形式型別是Tree，實際型別是MartianTree。
      Tree saturnianTree = new SaturnianTree();  // 形式型別是Tree，實際型別是SaturnianTree。

      TreeDoctor doctor = new TreeDoctor();

      // 樹醫生要去醫治不同的樹：
      doctor.cure(hardwood);
      doctor.cure(conifer);
      doctor.cure(venusianTree);
      doctor.cure(martianTree);
      doctor.cure(saturnianTree);
    }
  }
}