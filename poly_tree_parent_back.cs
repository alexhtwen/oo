using System;
namespace MyApplication
{
  class Tree  // Base class (parent)
  {
    public virtual void bloom()   // 要加virtual
    {
      Console.WriteLine("I am a virtual Tree, so I don't know whether I will bloom.");
    }
  }

  class Hardwood : Tree  // Derived class (child)
  {
    public override void bloom()   // 要加override
    {
      Console.WriteLine("I am a Hardwood, so I bloom every year.");
    }
    public void doExtraWorks()   // 子類別擴充的方法
    {
        Console.WriteLine("A Hardwood is doing extra works.");
    }
  }

  class Conifer : Tree  // Derived class (child)
  {
    public override void bloom()   // 要加override
    {
      Console.WriteLine("I am a Conifer, so I DON'T bloom.");
    }
    public void doExtraWorks()   // 子類別擴充的方法
    {
        Console.WriteLine("A Conifer is doing extra works.");
    }
  }

  class VenusianTree : Tree   // 金星上的樹
  {
    public override void bloom()   // 要加override
    {
      Console.WriteLine("Being a Venusian tree, I bloom once every 100 years.");
    }
    public void doExtraWorks()   // 子類別擴充的方法
    {
        Console.WriteLine("A VenusianTree is doing extra works.");
    }
  }

  class MartianTree : Tree   // 火星上的樹
  {
    public override void bloom()   // 要加override
    {
      Console.WriteLine("Being a Martian tree, I don't have the concept of blooming.");
    }
    public void doExtraWorks()   // 子類別擴充的方法
    {
        Console.WriteLine("A MartianTree is doing extra works.");
    }
  }

  class SaturnianTree : Tree   // 土星上的樹
  {
    public override void bloom()   // 要加override
    {
      Console.WriteLine("Being a Saturnian tree, I don't have any shape(ghostlymorphism).");
    }
    public void doExtraWorks()   // 子類別擴充的方法
    {
        Console.WriteLine("A SaturnianTree is doing extra works.");
    }
  }

  class TreeDoctor
  {
    public void cure(Tree tree)
    {
      Console.WriteLine("I am curing the " + tree + '.');
    }
  }

  class Program
  {
    static void Main(string[] args)
    {
      // Tree tree = new Tree();                      // Create a Tree object
      Tree hardwood = new Hardwood();              // Create a Hardwood object
      Tree conifer = new Conifer();                // Create a Conifer object
      Tree venusianTree = new VenusianTree();      // Create a VenusianTree object
      Tree martianTree = new MartianTree();        // Create a MartianTree object
      Tree saturnianTree = new SaturnianTree();    // Create a SaturnianTree object

      TreeDoctor doctor = new TreeDoctor();
      doctor.cure(hardwood);
      doctor.cure(conifer);
      doctor.cure(venusianTree);
      doctor.cure(martianTree);
      doctor.cure(saturnianTree);


      // tree.bloom();
      // saturnianTree.bloom();
      // Tree[] trees = {hardwood, conifer, venusianTree, martianTree, saturnianTree};
      // Random rnd = new Random();
      // int thisTree = rnd.Next(0, 5);
      // trees[thisTree].bloom();
      // 由於Tree類別並無doExtraWorks()，下列這行執行時會報錯。
      // trees[thisTree].doExtraWorks();
    }
  }
}