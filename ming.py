# 餅店明細
import datetime
products = {"C01": {'name': "栗子蛋糕", 'price': 160},
            "C02": {'name': "芒果蛋糕", 'price': 150},
            "C03": {'name': "瑞士蛋卷", 'price': 130},
            "C04": {'name': "奶油鬆餅", 'price': 110}}
print("======= 明細表 =======")

# 顯示菜單
print(f'{datetime.datetime.now()}')
for k, v in products.items():
    print(f'{k}\t {v["name"]}\t {v["price"]}')

# 客人選單
choice_count = 0
items = []
while True:
    choice = input("請輸入貨品編號 C01~C04(-1 停止輸入):")
    if choice == '-1':
        print(items)
        break
    choice = choice.strip().title()
    if products.get(choice) is None:
        continue


    qty = int(input("請輸入購買數量(1-1000):"))
    if qty < 1 or qty > 1000:
        print("請再輸入數量(1-1000):")
        continue
    print(f"{choice} {products[choice]['name']}  {qty} {products[choice]['price'] * qty}")
    items.append({choice: products[choice]['name'], "unit_price": products[choice]['price'], "quantity": qty , \
                      "subtotal": products[choice]['price'] * qty})

 # 計算總價
if items:
    total_price = 0
    for item in items:
        total_price += item["subtotal"]
    print(f'總價:{total_price}')

# 顧客付款和找零
    payment = total_price
    payments = 0
    while True:
        payments += int(input("請輸入顧客付款: "))
        print(f'顧客付款{payments}')
        if payments < payment:
            arr = payment - payments
            print(f'不好意思,您付的錢還差{arr}元,麻煩補足')
            continue
        if payments >= payment:
            exchange = payments - payment
        print(f'收您{payments}元,找您{exchange}元,謝謝!')
        break

# 統一編號
    code_needed = input("需要統一編號嗎?(y or n): ")
    if code_needed == "y" or code_needed == "Y":
        code = input("請輸入統編號:")
        print(f'統編號碼,{code},謝謝惠顧')
    else:
        if code_needed == "n" or code_needed == "N":
            print("謝謝惠顧,這是您的發票")
if items == []:
    print("謝謝光臨")