#include <stdio.h>

double GetBmi(double, double);

int main()
{

  char name[50] = "Alex Van";            // 姓名
  double height = 169;                   // 身高
  double weight = 67.5;                  // 體重
  double bmi = GetBmi(weight, height);   // 呼叫計算BMI函數
  printf("name: %s\t\tBMI: %.2lf\n", name, bmi);
  return 0;
}

double GetBmi(double weight, double height)
{
  height = height / 100;
  return weight / (height*height);
}