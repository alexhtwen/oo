using System;
namespace MyApplication
{
  class Tree  // Base class (parent)
  {
    public virtual void bloom()   // 要加virtual
    {
      Console.WriteLine("I am a virtual Tree, so I don't know whether I will bloom.");
    }
  }

  class Hardwood : Tree  // Derived class (child)
  {
    public override void bloom()   // 要加override
    {
      Console.WriteLine("I am a Hardwood, so I bloom every year.");
    }
    public void doExtraWorks()
    {
        Console.WriteLine("A Hardwood is doing extra works.");
    }
  }

  class Conifer : Tree  // Derived class (child)
  {
    public override void bloom()   // 要加override
    {
      Console.WriteLine("I am a Conifer, so I DON'T bloom.");
    }
    public void doExtraWorks()
    {
        Console.WriteLine("A Conifer is doing extra works.");
    }
  }

  class VenusianTree : Tree   // 金星上的樹
  {
    public override void bloom()   // 要加override
    {
      Console.WriteLine("Being a Venusian tree, I bloom once every 100 years.");
    }
    public void doExtraWorks()
    {
        Console.WriteLine("A VenusianTree is doing extra works.");
    }
  }

  class MartianTree : Tree   // 火星上的樹
  {
    public override void bloom()   // 要加override
    {
      Console.WriteLine("Being a Martian tree, I don't have the concept of blooming.");
    }
    public void doExtraWorks()
    {
        Console.WriteLine("A MartianTree is doing extra works.");
    }
  }

  class SaturnianTree : Tree   // 土星上的樹
  {
    public override void bloom()   // 要加override
    {
      Console.WriteLine("Being a Saturnian tree, I don't have any shape(ghostlymorphism).");
    }
    public void doExtraWorks()
    {
        Console.WriteLine("A SaturnianTree is doing extra works.");
    }
  }

  class SolarTree : Tree   // 太陽上的樹
  {
    public override void bloom()   // 要加override
    {
      Console.WriteLine("Being a Solar tree, I burned myself..");
    }
    public void doExtraWorks()
    {
        Console.WriteLine("A SolarTree is doing extra works.");
    }
  }

  class TreeDoctor
  {
    public void cure_sick(Tree tree)
    {
      Console.WriteLine("I am curing the " + tree + '.');
    }
  }


  class Program
  {
    static void Main(string[] args)
    {
      hardwood hardwood = new Hardwood();                 // Create a Hardwood object
      Conifer conifer = new Conifer();                    // Create a Conifer object
      VenusianTree venusianTreeTree = new VenusianTree(); // Create a VenusianTree object
      MartianTree martianTree = new MartianTree();        // Create a MartianTree object
      SaturnianTree saturnianTree = new SaturnianTree();  // Create a SaturnianTree object

      TreeDoctor doctor = new TreeDoctor();
      doctor.cure_sick(hardwood);
      doctor.cure_sick(conifer);
      doctor.cure_sick(venusianTree);
      doctor.cure_sick(martianTree);
      doctor.cure_sick(saturnianTree);

    //   tree.bloom();
    //   Tree[] trees = {hardwood, conifer, venusianTreeTree, martianTree, saturnianTree};
    //   trees[0] = new SolarTree();
    //   Random rnd = new Random();
    //   int thisTree = rnd.Next(0, 5);
    //   trees[thisTree].bloom();
    //   trees[thisTree].doExtraWorks();
    }

  }
}