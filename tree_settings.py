# tree_settings.py
import tree_classes

tree_infos = {'Earth': {'breed': 'ebony', 'age': 2_500},
              'Venus': {'breed': 'vitex', 'age': -3_000},
              'Mars': {'breed': 'mvule', 'age': 500_000_000},
              'Sun': {'breed': 'salix', 'age': 0}
              }

tree_on_earth = tree_classes.EarthlyTree(tree_infos['Earth']['breed'], tree_infos['Earth']['age'])
tree_on_venus = tree_classes.VenusianTree(tree_infos['Venus']['breed'], tree_infos['Venus']['age'])
tree_on_mars = tree_classes.MartianTree(tree_infos['Mars']['breed'], tree_infos['Mars']['age'])
tree_on_sun = tree_classes.SolarTree(tree_infos['Sun']['breed'], tree_infos['Sun']['age'])

trees = {tree_infos['Earth']['breed']: tree_on_earth,
         tree_infos['Venus']['breed']: tree_on_venus,
         tree_infos['Mars']['breed']: tree_on_mars,
         tree_infos['Sun']['breed']: tree_on_sun}
