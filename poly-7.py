from tree_settings import trees

print()
breed = input('Enter a tree breed: ').strip().lower()
tree = trees.get(breed)
if tree is not None:
    tree.grow()
    tree.reproduce()
    tree.get_sick()
    tree.die()
    print()
else:
    print('Oops, this tree is not in our list.  Maybe you misspelled it?')