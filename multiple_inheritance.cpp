#include <iostream>
using namespace std;

class Tree{
    public:
        string breed;
        Tree(string myBreed){
            breed = myBreed;
            cout << "Tree's constructor.  I am a " << this->breed << '.' << endl;
        }
    };

class Hardwood : public Tree{    // 繼承自Tree。
    public:
        Hardwood(string breed): Tree(breed){
            cout << "Hardwood's constructor." << endl << endl;
        }
    };

class Conifer : public Tree{      // 繼承自Tree。
    public:
        Conifer(string breed): Tree(breed){
            cout << "Conifer's constructor." << endl << endl;
        }

    };

class Timba : public Hardwood, public Conifer{    // 同時繼承自Hardwood及Conifer。
public:
    Timba(string breed): Hardwood(breed), Conifer(breed){
        cout << "Timba's constructor." << endl;
    }
};


int main(void){
    Timba timba("timba");
    return 0;
}