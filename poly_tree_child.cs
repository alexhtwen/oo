using System;
namespace MyApplication
{
  class Tree  // Base class (parent)
  {
  }

  class Hardwood : Tree  // Derived class (child)
  {
  }

  class Conifer : Tree  // Derived class (child)
  {
  }

  class VenusianTree : Tree   // 金星樹
  {
  }

  class MartianTree : Tree   // 火星樹
  {
  }

  class SaturnianTree : Tree   // 土星樹
  {
  }

  class TreeDoctor    // // 新增這個「樹醫生」類別。
  {
    public void cure(Tree tree)  // 我要的參數是「通用型的樹」，而不是某個特定樹種。
    {
      Console.WriteLine("I am curing the " + tree + '.');
    }
  }

  class Program
  {
    static void Main(string[] args)
    {
      hardwood hardwood = new Hardwood();                 // 「形式型別」和「實際型別」都是Hardwood。
      Conifer conifer = new Conifer();                    // 「形式型別」和「實際型別」都是Conifer。
      VenusianTree venusianTreeTree = new VenusianTree(); // 「形式型別」和「實際型別」都是VenusianTree。
      MartianTree martianTree = new MartianTree();        // 「形式型別」和「實際型別」都是MartianTree。
      SaturnianTree saturnianTree = new SaturnianTree();  // 「形式型別」和「實際型別」都是SaturnianTree。

      TreeDoctor doctor = new TreeDoctor();

      // 樹醫生要去醫治不同的樹了。但，可以嗎？
      doctor.cure(hardwood);
      doctor.cure(conifer);
      doctor.cure(venusianTree);
      doctor.cure(martianTree);
      doctor.cure(saturnianTree);
    }
  }
}