using System;
namespace MyApplication
{
  public enum EmployeeType
  {
      Manager,
      Administrative,
      General
  }

  public abstract class Employee
  {
      public string Name { get; set; }
      public EmployeeType Type { get; protected set; }
  }

  public class EmployeeAdministrative : Employee
  {
    public EmployeeAdministrative()
    {
      this.Type = EmployeeType.Administrative;
    }
  }

  public class EmployeeManager : Employee
  {
    public EmployeeManager()
    {
      this.Type = EmployeeType.Manager;
    }
  }

  public class EmployeeGeneral : Employee
  {
    public EmployeeGeneral()
    {
      this.Type = EmployeeType.General;
    }
  }

  public class Payments
  {
    public double Pay(EmployeeAdministrative employee) => 100;
    public double Pay(EmployeeManager employee) => -1000;
    public double Pay(EmployeeGeneral employee) => 200;
  }

  class Program
  {
    static void Main(string[] args)
    {
      Employee me = new EmployeeManager { Name = "Me" };
      Payments payments = new Payments();
      Console.WriteLine(payments.Pay(me));
    }
  }
}