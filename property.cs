using System;
namespace MyApplication
{
  class Tree
  {
    private string breed;  // 這是private attribute。
    public string Breed    // 這是public property。
    {
      // 將getter和setter包在property裡面。
      get { return breed; }   // getter
      // 透過public property修改private attribute。
      set { breed = value; }  // setter
    }
  }

  class Program
  {
    static void Main(string[] args)
    {
      Tree tree = new Tree();
      tree.Breed = "cedar";   // 直接賦值，表面上沒有使用getter。
      Console.WriteLine(tree.Breed);   // 直接取值，看不到setter影子。
    }
  }
}